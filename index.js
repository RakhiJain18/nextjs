var mongoose = require('mongoose');
const express = require('express')
const BodyParser = require("body-parser");
const app = express()
const port = 3000
//Set up default mongoose connection
var mongoDB = 'mongodb://localhost:27017/nodejs';
app.use(BodyParser.urlencoded({extended: false}));
app.use(BodyParser.json());
//register api

const auth = require('./router/api/auth')
app.use("/api/auth",auth)
// db connection
mongoose.connect(mongoDB, { useUnifiedTopology: true, useNewUrlParser: true, useFindAndModify: false })
.then((data)=>{console.log("connected successfully")})
.catch((err)=>{console.log('error')});
mongoose.set('useCreateIndex', true);
app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
    
  }) 
  