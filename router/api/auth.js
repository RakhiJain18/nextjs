const express = require('express')
const router = express.Router();
const Person = require('../../modules/user')

router.post('/register',async(req,res)=>{
  let person = await Person.findOne({email:req.body.email})
  if(person){
    return res.status(400).json({EmailError:'Email is Alerady Exist'})
  }else{
    const newperson = new Person({
        name:req.body.name,
        email:req.body.email
    })
    newperson.save()
    .then((person)=>res.json(person))
    .catch(err=>console.log(err));

  }
});

router.get('/user',async(req,res)=>{
  let person = await Person.find().then(products=>{
    res.status(200).json(products)
  })
});


module.exports = router